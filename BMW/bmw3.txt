Barely on the road and the all-new BMW 3 Series Sedan is already leaving everything behind it, including conventions and expectations.
Once again the icon displays how to reinvent itself.
After all, with the pioneering design language, it stands for the dawning of a new era.
Propelling the ultimate sports saloon are even more powerful and efficient engines.
And a new force � simply say �Hey BMW� and the BMW 3 Series recognises your voice and heeds your every word.

-5 good reasons for the BMW 3 Series Sedan:

1.Recognises your voice and listens to your wishes

2.With up to 374 horsepower in the M340i xDrive more dynamic than ever

3.Honed design meets the wish for sheer driving pleasure

4.BMW Laserlight illuminates the road up to 530 m ahead

5.Access to the vehicle via smartphone